<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class controllerdbuser extends Controller
{
	public function checkLogin(Request $request){
		$data = $request->all();
		$user = $data['username'];
		$pass = $data['password'];
		$array = array(
			'Auser' => $user,
			'Apass' => $pass
		);
		$ans = DB::table('authorities')->where($array)->get();
		$row_count = count($ans);
		if($row_count == 1){
          return view('/home1');
			
		}
		else{
			$array = array(
				'Duser' => $user,
				'Dpass' => $pass
			);
			$ans = DB::table('doctor')->where($array)->get();
			$row_count = count($ans);
			if($row_count == 1){
				return view('doctor/doctorhome', ['ans' => $ans]);
			}
			else
			{
				$array = array(
					'Cuser' => $user,
					'Cpass' => $pass
				);
				$ans = DB::table('customer')->where($array)->get();
				$row_count = count($ans);
				if($row_count == 1){

					return view('customer/customerhome', ['ans' => $ans]);

				}
				else
				{
					$message = "fill";
					echo "<script type='text/javascript'>alert('$message');</script>";
					return view('/login');
				}
			}
		}

	}

	public function test(){
		return view('/test');
	}
	public function insert_data(Request $request)
	{
		$data = $request->all();

		$result = array(
			'Auser' => $data['username'],
			'Apass' => $data['password'],
			'Afirst_name' => $data['fname'],
			'Alast_name' => $data['lname'],
			'Aadd' => $data['add'],
			'Abirth' => $data['bday'],
			'Aphone' => $data['phone'],
			'Aphotp' => $data['photo'],
			'info_status' => '3'		
		);
		DB::table('authorities')->insert($result);
		return redirect('adduser');
	}

	public function insert_data_doctor(Request $request)
	{
		$data = $request->all();

		$result = array(
			'Duser' => $data['username'],
			'Dpass' => $data['password'],
			'Dfirst_name' => $data['fname'],
			'Dlast_name' => $data['lname'],
			'Dadd' => $data['add'],
			'Dbirth' => $data['bday'],
			'Dphone' => $data['phone'],
			'Dphoto' => $data['photo'],
			'info_status' => '2'	
		);
		DB::table('doctor')->insert($result);
		return redirect('addDoctor');
	}
	public function insert_data_customer(Request $request)
	{
		$data = $request->all();

		$result = array(
			'Cuser' => $data['username'],
			'Cpass' => $data['password'],
			'Cfirst_name' => $data['fname'],
			'Clast_name' => $data['lname'],
			'Cadd' => $data['add'],
			'Cbirth' => $data['bday'],
			'CDrug_Allergy' => $data['cdrug'],
			'Cphone' => $data['phone'],
			'info_status' => '1'			
		);
		DB::table('customer')->insert($result);
		return redirect('addCustomer');
	}
	public function insert_data_addtreatment_rate(Request $request)
	{
		$data = $request->all();

		$result = array(
			'name' => $data['name'],
			'price' => $data['price'],

		);
		DB::table('treatment_rate')->insert($result);
		return redirect('/selecttreatment_rate');
	}

	public function selectuser(){
		$users = DB::table('authorities')->get();
		return view('backend/selectuser' , ['users' => $users]);
	}
	public function selectdoctor(){
		$users = DB::table('doctor')->get();
		return view('backend/selectdoctor' , ['users' => $users]);
	}
	public function selectcustomer(){
		$users = DB::table('customer')->get();
		return view('backend/selectcustomer' , ['users' => $users]);
	}
	public  function selecttreatment_rate(){
		$users = DB::table('treatment_rate')->get();
		return view('/selecttreatment_rate' , ['users' => $users]);
	}

	public function delete_data_treatment_rate($id){
		DB::table('treatment_rate')->where('TID',$id)->delete();
		return redirect('/selecttreatment_rate');
	}




	public function edit_data_user($id){
		$users = DB::table('authorities')->where('AID',$id)->get();
		return view('backend/edituser' , ['user' => $users]);
	}
	public function edit_data_doctor($id){
		$users = DB::table('doctor')->where('DID',$id)->get();
		return view('backend/editDoctor' , ['user' => $users]);
	}
	public function edit_data_customer($id){
		$users = DB::table('customer')->where('Cus_ID',$id)->get();
		return view('backend/editCustomer' , ['user' => $users]);
	}
	public  function edit_data_treatment_rate($id){
		$users = DB::table('treatment_rate')->where('TID',$id)->get();
		return view('/edittreatment_rate' , ['user' => $users]);
	}



	public function addtreatment_rate(){
		return view('backend/addtreatment_rate');
	}

	public function test55(){
		return view('/test');
	}

	public function update_data_user(Request $request){

		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'Auser' => $data['username'],
			'Apass' => $data['password'],
			'Afirst_name' => $data['fname'],
			'Alast_name' => $data['lname'],
			'Aadd' => $data['add'],
			'Abirth' => $data['bday'],
			'Aphone' => $data['phone'],
			'Aphotp' => $data['photo'],
			'info_status' => '3'

		);
		DB::table('authorities')->where('AID',$id)->update($result);
		return redirect('/selectuser');
	}
	public function update_data_doctor(Request $request){

		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'Duser' => $data['username'],
			'Dpass' => $data['password'],
			'Dfirst_name' => $data['fname'],
			'Dlast_name' => $data['lname'],
			'Dadd' => $data['add'],
			'Dbirth' => $data['bday'],
			'Dphone' => $data['phone'],
			'Dphoto' => $data['photo'],
			'info_status' => '2'	

		);
		DB::table('doctor')->where('DID',$id)->update($result);
		return redirect('selectdoctor');
	}
	public function update_data_customer(Request $request){

		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'Cuser' => $data['username'],
			'Cpass' => $data['password'],
			'Cfirst_name' => $data['fname'],
			'Clast_name' => $data['lname'],
			'Cadd' => $data['add'],
			'Cbirth' => $data['bday'],
			'CDrug_Allergy' => $data['cdrug'],
			'Cphone' => $data['phone'],
			'info_status' => '1'		

		);
		DB::table('customer')->where('Cus_ID',$id)->update($result);
		return redirect('selectcustomer');
	}

	public function update_data_treatment_rate(Request $request){

		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'name' => $data['name'],
			'price' => $data['price'],

		);
		DB::table('treatment_rate')->where('TID',$id)->update($result);
		return redirect('/selecttreatment_rate');
	}

	public function test3(Request $request)
	{
		$data = $request->all();
		return view('/home1');
	}
	public function adddaytable_user(){
		$ans = DB::table('doctor')->get();
		$users = DB::table('tabledate')->get();
		return view('backend/about', ['ans' => $ans,'users' => $users]);
		//return view('/home1');
	}
	public function deletedaytable_user($id)
	{
		DB::table('tabledate')->where('id',$id)->delete();
		
		return redirect('/adddaytable_user');
	}
	public function insertday_user(Request $request)
	{
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'day' => $data['day'],
			'time' => $data['time'],
			'id_doc' => $data['id']
		);
		DB::table('tabledate')->insert($result);
		$ans = DB::table('doctor')->get();
		$users = DB::table('tabledate')->get();
		return view('backend/about', ['ans' => $ans,'users' => $users]);
	}



}
