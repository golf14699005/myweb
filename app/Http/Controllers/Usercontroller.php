<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class Usercontroller extends Controller
{
    public function home(){
    	$users = DB::table('user')->get();
    	return view('home' , ['users' => $users]);
    }

    public function home1(){
    	return view('home1');
    }
    public function about(){
    	return view('about');
    }
    public function services(){
    	return  view('services');
    }
    
    public function adduser(){
    	return view('backend/adduser');
    }

    public function addDoctor(){
      return view('backend/addDoctor');
  }
  public function addCustomer(){
      return view('backend/addCustomer');
  }


     /*public function addSchedule(){
     	return view('/addSchedule');
     }*/


     public function insert_data(Request $request)
     {
      $data = $request->all();

      $result = array(
         'user' => $data['username'],
         'pass' => $data['password'],
         'first_name' => $data['fname'],
         'last_name' => $data['lname'],
         'info_status' => '0'
     );
      DB::table('user')->insert($result);
      return redirect('home');
  }

  public function insert_data2(Request $request)
  {
      $data = $request->all();

      $result = array(
         'Auser' => $data['username'],
         'Apass' => $data['password'],
         'Afirst_name' => $data['fname'],
         'Alast_name' => $data['lname'],
         'Aadd' => $data['add'],
         'Abirth' => $data['bday'],
         'Aphone' => $data['phone'],
         'Aphotp' => '59011212239',
         'info_status' => '0'		
     );
      DB::table('authorities')->insert($result);
      return redirect('adduser');
  }

  public function delete_data_user($id){
   DB::table('authorities')->where('AID',$id)->delete();
   return redirect('selectuser');
}
public function delete_data_doctor($id){
   DB::table('doctor')->where('DID',$id)->delete();
   return redirect('selectdoctor');
}
public function delete_data_customer($id){
   DB::table('customer')->where('Cus_ID',$id)->delete();
   return redirect('selectcustomer');
}


public function edit_data_user($id){
   $users = DB::table('authorities')->where('AID',$id)->get();
   return view('edituser' , ['user' => $users]);
}
public function login(){
    return view('/login');
}





public function update_data(Request $request){

  $data = $request->all();
  $id = $data['id'];
  $result = array(
     'user' => $data['username'],
     'pass' => $data['password'],
     'first_name' => $data['fname'],
     'last_name' => $data['lname'],
     'info_status' => '0'
 );
  DB::table('user')->where('id',$id)->update($result);
  return redirect('home');
}

}
