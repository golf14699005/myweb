<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class customerController extends Controller
{
    //
    public function customerview(Request $request)
    {
    	$ans = $request->all();
      return view('customer/customerhome', ['ans' => $ans]);

    }
    public function edit_customer($id){
      $ans = DB::table('customer')->where('Cus_ID',$id)->get();
      return view('customer/editcustomer' , ['ans' => $ans]);
    }
    public function login(){
        return view('/login');
    }
    public function update_data_customer(Request $request){

        $data = $request->all();
        $user = $data['username'];
        $pass = $data['password'];
        $id = $data['id'];
        $result = array(
            'Cuser' => $data['username'],
            'Cpass' => $data['password'],
            'Cfirst_name' => $data['fname'],
            'Clast_name' => $data['lname'],
            'Cadd' => $data['add'],
            'Cbirth' => $data['bday'],
            'CDrug_Allergy' => $data['cdrug'],
            'Cphone' => $data['phone'],
            'info_status' => '1'        

        );
        DB::table('customer')->where('Cus_ID',$id)->update($result);
        $array = array(
            'Cuser' => $user,
            'Cpass' => $pass
        );
        $ans = DB::table('customer')->where($array)->get();
        $row_count = count($ans);
            if($row_count == 1){

                return view('customer/customerhome', ['ans' => $ans]);
            }
    
        }
    public function checktabledoc($id){
        $ans = DB::table('customer')->where('Cus_ID',$id)->get();
        $users = DB::table('tabledate')->get();
         $doc = DB::table('doctor')->get();
        return view('customer/checktabledoc', ['ans' => $ans,'users' => $users,'doc'=>$doc]);
    }
    public function customerhome($id){
        $ans = DB::table('customer')->where('Cus_ID',$id)->get();
        return view('customer/customerhome', ['ans' => $ans]);
    }
    public function selecttreatment_rate_cus($id){
        $ans = DB::table('customer')->where('Cus_ID',$id)->get();
        $users = DB::table('treatment_rate')->get();
        return view('customer/selecttreatment_rate', ['ans' => $ans,'users' => $users]);

    }
    public function Schedule($id)
    {
        $ans = DB::table('customer')->where('Cus_ID',$id)->get();
        $users = DB::table('tabledate')->get();
         $doc = DB::table('doctor')->get();
        return view('customer/Schedule', ['ans' => $ans,'users' => $users,'doc'=>$doc]);
    }

    public function inserdate_table(Request $request)
    {
        $data = $request->all();
        $cid = $data['id'];
        $new_date = date('Y-m-d', strtotime($request['TDate']));
        $result = array(
            'Tdate' => $new_date,
            'Ttime' => $data['TTime'],
            'cid' => $data['id'],
            'did' => $data['did']
        );
        DB::table('date_table')->insert($result);
         $ans = DB::table('customer')->where('Cus_ID',$cid)->get();
         $users = DB::table('date_table')->where('cid',$cid)->get();
         $doc = DB::table('doctor')->get();
         return view('customer/deedate', ['ans' => $ans,'users' => $users,'did' => $doc]);
    }
     public function deedate($id){
        $ans = DB::table('customer')->where('Cus_ID',$id)->get();
        $users = DB::table('date_table')->where('cid',$id)->get();
        $did = DB::table('doctor')->get();
        return view('customer/deedate', ['ans' => $ans,'users' => $users,'did' => $did]);

    }
    public function delete_data_schedule($id,$idd)
    {
         DB::table('date_table')->where('id',$id)->delete();
          $ans = DB::table('customer')->where('Cus_ID',$idd)->get();
        $users = DB::table('date_table')->where('cid',$idd)->get();
        $did = DB::table('doctor')->get();
        return view('customer/deedate', ['ans' => $ans,'users' => $users,'did' => $did]);
    }
    public function edit_data_schedule($id,$idd)
    {
          DB::table('date_table')->where('id',$id)->delete();
          $ans = DB::table('customer')->where('Cus_ID',$idd)->get();
        $users = DB::table('tabledate')->get();
         $doc = DB::table('doctor')->get();
        return view('customer/Schedule', ['ans' => $ans,'users' => $users,'doc'=>$doc]);
    }

}
