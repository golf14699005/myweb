<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class doctorController extends Controller
{
    //
	public function edit_doctor($id){
		$users = DB::table('doctor')->where('DID',$id)->get();
		return view('doctor/editdoctor' , ['user' => $users]);
	}
	public function update_doctor(Request $request){

		$data = $request->all();
		$user = $data['username'];
		$pass = $data['password'];
		$id = $data['id'];
		$result = array(
			'Duser' => $data['username'],
			'Dpass' => $data['password'],
			'Dfirst_name' => $data['fname'],
			'Dlast_name' => $data['lname'],
			'Dadd' => $data['add'],
			'Dbirth' => $data['bday'],
			'Dphone' => $data['phone'],
			'Dphoto' => $data['photo'],
			'info_status' => '2'	

		);
		DB::table('doctor')->where('DID',$id)->update($result);
		$array = array(
			'Duser' => $user,
			'Dpass' => $pass
		);
		$ans = DB::table('doctor')->where($array)->get();
		$row_count = count($ans);
		if($row_count == 1){
			return view('doctor/doctorhome', ['ans' => $ans]);
		}
	}

	public function selectcustomer_doc($id){
		$usersdoc = DB::table('doctor')->where('DID',$id)->get();
		$usercus = DB::table('customer')->get();
		return view('doctor/selectcustomer' , ['user' => $usersdoc,'usercus' => $usercus]);
	}
	public function doctrohome($id){
		$ans = DB::table('doctor')->where('DID',$id)->get();
		return view('doctor/doctorhome', ['ans' => $ans]);
	}
	public function selecttreatment_rate_doc($id){
		$ans = DB::table('doctor')->where('DID',$id)->get();
		$users = DB::table('treatment_rate')->get();
		return view('doctor/selecttreatment_rate', ['ans' => $ans,'users' => $users]);

	}
	public function adddaytable($id){
		$ans = DB::table('doctor')->where('DID',$id)->get();
		$users = DB::table('tabledate')->where('id_doc',$id)->get();
		return view('doctor/adddaytable', ['ans' => $ans,'users' => $users]);
	}
	public function insertday(Request $request)
	{
		$data = $request->all();
		$id = $data['id'];
		$result = array(
			'day' => $data['day'],
			'time' => $data['time'],
			'id_doc' => $data['id']
		);
		DB::table('tabledate')->insert($result);
		$ans = DB::table('doctor')->where('DID',$id)->get();
		$users = DB::table('tabledate')->where('id_doc',$id)->get();
		return view('doctor/adddaytable', ['ans' => $ans,'users' => $users]);
	}
	public function deletedaytable($id,$idd)
	{
		DB::table('tabledate')->where('id',$id)->delete();
		$ans = DB::table('doctor')->where('DID',$idd)->get();
		$users = DB::table('tabledate')->where('id_doc',$idd)->get();
		return view('doctor/adddaytable', ['ans' => $ans,'users' => $users]);
	}
	public function addredord($id)
	{
		$ans = DB::table('doctor')->where('DID',$id)->get();
		$tre = DB::table('treatment_rate')->get();
		$cus = DB::table('customer')->get();
		return view('doctor/record', ['ans' => $ans,'cus' => $cus,'tre'=>$tre]);
	}
	public function insertrecord(Request $request)
	{
		$data = $request->all();
		$cid = $data['cid'];
		$tid = $data['tid'];
		$did = $data['did'];
		$monney = $data['money'];
		$ctable = DB::table('customer')->where('Cus_ID',$cid)->get();
		$ttable = DB::table('treatment_rate')->where('TID',$tid)->get();
		$mmoney = $ttable[0]->price;
		$mtotal = $ctable[0]->money;
		if($mtotal==null){
			$temp1 = (int)$mmoney;
			$temp2 = (int)$monney;
			$temp3 = $temp1-$temp2;
			$mtotal = (string)$temp3;
		}
		else
		{
			$temp1 = (int)$mtotal;
			$temp2 = (int)$mmoney;
			$temp3 = $temp1+$temp2;
			$temp4 = (int)$monney;
			$temp5 = $temp3-$temp4;
			$mtotal = (string)$temp5;
		}
		$resultr = array(
			'list_id' => $data['tid'],
			'c_id' => $data['cid'],
			'd_id' => $data['did'],
			'Pay' => $data['money'],
			'day' => date("Y-m-d")
		);
		DB::table('record')->insert($resultr);
		$resultc = array(
			'Cuser' => $ctable[0]->Cuser,
			'Cpass' => $ctable[0]->Cpass,
			'Cfirst_name' => $ctable[0]->Cfirst_name,
			'Clast_name' => $ctable[0]->Clast_name,
			'Cadd' => $ctable[0]->Cadd,
			'Cbirth' => $ctable[0]->Cbirth,
			'CDrug_Allergy' => $ctable[0]->CDrug_Allergy,
			'Cphone' => $ctable[0]->Cphone,
			'money' => $mtotal,
			'info_status' => '1'		
		);
		DB::table('customer')->where('Cus_ID',$cid)->update($resultc);

		$ans = DB::table('doctor')->where('DID',$did)->get();
		$tre = DB::table('treatment_rate')->get();
		$cus = DB::table('customer')->get();
		return view('doctor/record', ['ans' => $ans,'cus' => $cus,'tre'=>$tre]);
	}

}
