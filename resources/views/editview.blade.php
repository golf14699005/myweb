<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
 	<script src="{{ asset('js/jquery.js')}}" type="text/javascript" ></script>
</head>
<body>
	
 	<div class="container mt-5 mb-5">
 		<div class="row">
 			<form action="/update_data" method="POST" class="pt-5 pb-5">
 				<input type="hidden" name="id" value="<?=$user[0]->id?>">
 				{{ csrf_field() }}
 				<div class="form-group">
 					<label >Username</label>
 					<input type="text" class="form-control" id="username" value="<?=$user[0]->user?>" placeholder="Enter Username" name="username">	
 				</div>
 				<div class="form-group">
 					<label >Password</label>
 					<input type="password" class="form-control" id="password" value="<?=$user[0]->pass?>"  placeholder="Enter Password" name="password">		
 				</div>
 				<div class="form-group">
 					<label >First Name</label>
 					<input type="text" class="form-control" id="fname" value="<?=$user[0]->first_name?>" placeholder="Enter First Name" name="fname">				
 				</div>
 				<div class="form-group">
 					<label >Last Name</label>
 					<input type="text" class="form-control" id="lname" value="<?=$user[0]->last_name?>" placeholder="Enter Last Name" name="lname">				
 				</div>
 				<button type="submit" class="btn btn-primary">Submit</button>
 			</form>

 		</div>

 	</div>

		<script src="{{ asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript" ></script>

 	<script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript" ></script>
</body>
</html>