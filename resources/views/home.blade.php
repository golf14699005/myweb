 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title></title>
 	<link rel="stylesheet" href="">
 	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
 	<script src="{{ asset('js/jquery.js')}}" type="text/javascript" ></script>

 </head>
 <body>

 	<p id="demo"></p>

 	<script>
 		var d = new Date();
 		document.getElementById("demo").innerHTML = d;
 	</script>

 	<form action=""  method="POST" class="pt-5 pb-5">
 		<label>Date: </label>
 		<input type="date" name="">
 		<input type="time" name="">
 	</form>

 	<div class="container mb-5">
 		<h1 class="display-4 mt-4 mb-5"> Add User</h1>
 		<div class="row">
 			<div class="form-row w-100">
 				<div class="table-responsive">

 					


 					<!--<button style="height:100%;width:100%">".$user->id."</button>-->
 				</style></button>
 				<table class="table table-hover " border="1">
 					<thead > 

 						<th>#</th>
 						<th>Username </th>
 						<th>First Name</th>
 						<th>Last Name </th>
 						<th></th>
 						<th></th>
 					</thead>	
 					<tbody>
 						<?php
 						if(count($users) > 0){
 								//$i = 1 ;
 							foreach ($users as $user) {
 								echo "<tr>";
 								echo "<td>".$user->id."</td>";
 								echo "<td><button style='height:100%;width:100%''>".$user->user."</td>";
 								echo "<td><button style='height:100%;width:100%''>".$user->first_name."</td>";
 								echo "<td><button style='height:100%;width:100%''>".$user->last_name."</td>";
 								echo "<td><a href='delete_data/".$user->id."' title=''>Delete</a></td>";
 								echo "<td><a href='edit_data/".$user->id."' title=''>Edit</a></td>";

 								echo "</tr>";
 								//	$i++;
 							}
 						}
 						else
 						{
 							echo "<tr><td align='center' colspan='4'>Not Found Data </td></tr>";
 						}

 						?>				
 					</tbody>

 				</table>					
 			</div>				
 		</div>			
 	</div>
 </div>

 <div class="container mt-5 mb-5">
 	<div class="row">
 		<form action="/insert_data1" method="POST" class="pt-5 pb-5">
 			{{ csrf_field() }}
 			<div class="form-group">
 				<label >Username</label>
 				<input type="text" class="form-control" id="username"  placeholder="Enter Username" name="username">	
 			</div>
 			<div class="form-group">
 				<label >Password</label>
 				<input type="password" class="form-control" id="password"  placeholder="Enter Password" name="password">		
 			</div>
 			<div class="form-group">
 				<label >First Name</label>
 				<input type="text" class="form-control" id="fname"  placeholder="Enter First Name" name="fname">				
 			</div>
 			<div class="form-group">
 				<label >Last Name</label>
 				<input type="text" class="form-control" id="lname"  placeholder="Enter Last Name" name="lname">				
 			</div>
 			<button type="submit" class="btn btn-primary">Submit</button>
 		</form>

 	</div>

 </div>


 <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript" ></script>

 <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript" ></script>
</body>
</html>