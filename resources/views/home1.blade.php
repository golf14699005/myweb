<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="css/demos/demo-digital-agency.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="css/skins/skin-digital-agency.css"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">

	<!-- Head Libs -->
	<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body>

	<div class="body">
		<header id="header" class="header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '0'}">
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<a href="/home1">
										<img alt="Porto" width="131" height="40" src="img/demos/digital-agency/logo-digital-agency.png">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-stripe">
									<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li>
													<a class="nav-link active" href="/home1">
														Home
													</a>
												</li>
												<li>
													<a class="nav-link" href="/adddaytable_user">
														ตารางนัด
													</a>
												</li>

												<li>
													<a class="nav-link" href="/selecttreatment_rate">
														อัตราการรักษา
													</a>
												</li>
												<li>
													<a class="nav-link" href="/selectuser">
														แก้ไขข้อบุคคน
													</a>
												</li>
												<li>
													<a class="nav-link" href="/adduser">
														เพิ่มข้อบุคคน
													</a>
												</li>
												<li>
													<a class="nav-link" href="/login">
														ออกจากระบบ
													</a>
												</li>
											</ul>
										</nav>
									</div>

									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div role="main" class="main">

			<div class="slider-container rev_slider_wrapper" style="height: 600px;">
				<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 800, 'gridheight': 600}">
					<ul>
						<li data-transition="fade">

							<img src="https://cosdentbyslc.com/wp-content/uploads/2017/11/veneer.png"  
							alt=""
							data-bgposition="center center" 
							data-bgfit="cover" 
							data-bgrepeat="no-repeat" 
							data-kenburns="on"
							data-duration="9000"
							data-ease="Linear.easeNone"
							data-scalestart="115"
							data-scaleend="100"
							data-rotatestart="0"
							data-rotateend="0"
							data-offsetstart="0 -200"
							data-offsetend="0 200"
							data-bgparallax="0"
							class="rev-slidebg">

							<div class="tp-caption tp-caption-overlay tp-caption-overlay-primary main-label"
							data-x="center"
							data-y="265"
							data-start="1000"
							data-whitespace="nowrap"						 
							data-transform_in="y:[100%];s:500;"
							data-transform_out="opacity:0;s:500;"
							data-mask_in="x:0px;y:0px;">WELCOME TO มอสอ็อดคลินิกทันตกรรม</div>

							<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="center"
							data-y="358"
							data-start="2000"
							data-transform_in="y:[100%];opacity:0;s:500;">Project Webpro 1018</div>

						</li>
						<li data-transition="fade">
							<img src="https://cosdentbyslc.com/wp-content/uploads/2017/11/damon-clear.png"  
							alt=""
							data-bgposition="center center" 
							data-bgfit="cover" 
							data-bgrepeat="no-repeat" 
							data-kenburns="on"
							data-duration="9000"
							data-ease="Linear.easeNone"
							data-scalestart="115"
							data-scaleend="100"
							data-rotatestart="0"
							data-rotateend="0"
							data-offsetstart="0 400px"
							data-offsetend="0 -400px"
							data-bgparallax="0"
							class="rev-slidebg">

							<div class="tp-caption tp-caption-overlay tp-caption-overlay-primary main-label"
							data-x="center"
							data-y="265"
							data-start="1000"
							data-whitespace="nowrap"						 
							data-transform_in="y:[100%];s:500;"
							data-transform_out="opacity:0;s:500;"
							data-mask_in="x:0px;y:0px;">มอสอ็อดคลินิกทันตกรรม</div>

							<div class="tp-caption tp-caption-overlay-opacity bottom-label"
							data-x="center"
							data-y="358"
							data-start="2000"
							data-transform_in="y:[100%];opacity:0;s:500;">รักษาช่องปากคุณ</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="container">
				<div class="row justify-content-center mt-5">
					<div class="col-lg-10">

						<div class="tabs tabs-bottom tabs-center tabs-simple mt-2 mb-3">
							<ul class="nav nav-tabs">
								<li class="nav-item active">
									<a class="nav-link" href="#tabsNavigationSimpleIcons1" data-toggle="tab">
										<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
											<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
												<span class="box-content p-0 m-0">
													<i class="icon-featured icon-bulb icons"></i>
												</span>
											</span>
										</span>									
										<p class="mb-0 pb-0">ทันตกรรมพื้นฐาน</p>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#tabsNavigationSimpleIcons2" data-toggle="tab">
										<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
											<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
												<span class="box-content p-0 m-0">
													<i class="icon-featured icon-mustache icons"></i>
												</span>
											</span>
										</span>									
										<p class="mb-0 pb-0">ฟอกสีฟัน</p>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#tabsNavigationSimpleIcons3" data-toggle="tab">
										<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
											<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
												<span class="box-content p-0 m-0">
													<i class="icon-featured icon-puzzle icons"></i>
												</span>
											</span>
										</span>									
										<p class="mb-0 pb-0">จัดฟัน</p>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#tabsNavigationSimpleIcons4" data-toggle="tab">
										<span class="featured-boxes featured-boxes-style-6 p-0 m-0">
											<span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
												<span class="box-content p-0 m-0">
													<i class="icon-featured icon-rocket icons"></i>
												</span>
											</span>
										</span>									
										<p class="mb-0 pb-0">ทันตกรรมด้านอื่นๆ</p>
									</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tabsNavigationSimpleIcons1">
									<div class="text-center">
										<p>ทำฟัน ตรวจฟัน หรือ ศัพท์ทางการเรียกว่า ทันตกรรม การรักษา ทั่วไป (General Dentistry)
											ถือการทำฟันพื้นฐาน หรือ ทันตกรรมพื้นฐาน เป็นการทำฟันทั่วไป General Procedure
										ซึ่งถือเป็นพื้นฐานที่ทันตแพทย์ทุกๆคนจะสามารถทำได้เพราะเป็นขั้นตอนเบื้องต้น</p>
									</div>
								</div>
								<div class="tab-pane" id="tabsNavigationSimpleIcons2">
									<div class="text-center">
										<p>นวัตกรรมใหม่ที่สะดวกและรวดเร็วในการทำให้ฟันขาว
											โดยนำนวัตกรรมของระบบ cool light (คลูไลท์)  ที่ทำให้ฟันขาวขึ้นภายในระยะเวลาเพียง 45นาที กับชุด Easy white
											จึงเป็นทางเลือกของประสบการณ์ใหม่ที่ทำให้ฟันของคุณขาวขึ้น
										และกว่า 80% ของผู้ทำไม่พบเสียว แต่ทั้งนี้ต้องขึ้นอยู่กับสภาพฟันของแต่ล่ะบุคคลด้วย</p>
									</div>
								</div>
								<div class="tab-pane" id="tabsNavigationSimpleIcons3">
									<div class="text-center">
										<p>จัดฟัน ควรจัดเมื่อไหร่ และฟันแบบไหนที่ควรจัดฟัน คำถามยอดฮิตก่อนจัดฟัน วันนี้คลินิก
											จึงรวบรวมหาคำตอบมาให้เบื้องต้นว่าเมื่อพบฟัน 7 แบบนี้
											ก็สามารถพิจารณาและเป็นไปได้ให้รีบพบทันตแพทย์เพื่อปรึกษา
										และวางแผนเพื่อการรักษาที่ถูกต้องนะคะ</p>
									</div>
								</div>
								<div class="tab-pane" id="tabsNavigationSimpleIcons4">
									<div class="text-center">
										<p>รายการทันตกรรมที่ คลินิกทันตกรรม คอสเดนท์
											คลินิกทันตกรรมคอสเดนท์ เป็นคลินิกทันตกรรมที่ทำการรักษาทุกประเภท
										โดยมีจุดเด่นที่ทางคลินิกเน้นมากคือ เรื่องของรอยยิ้มที่สวยงาม</p>
									</div>
								</div>
							</div>
						</div>

						<p class="text-center">
							<a class="btn btn-light mt-3 mb-3" href="">Learn More <i class="fas fa-angle-right pl-1"></i></a>
						</p>
					</div>
				</div>

			</div>
			<section class="section section-default section-default-scale-8">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 class="mb-0 mt-0 font-weight-semibold text-light">จัดฟัน ดัดฟัน ต้อนรับซัมมเอร์:</h2>
							<p class="lead mb-3">การจัดฟันด้วยเครื่องมือโลหะ (METAL BRACES)</p>
							<div class="divider divider-primary divider-small divider-small-center">
								<hr>
							</div>
							<p class="mb-0 text-light">ป็นการจัดฟันที่รู้จักมากที่สุดกว่าได้ รวมถึงได้รับความนิยมมากที่สุด มักเห็นกันได้ตามทั่วไปโดยใช้เครื่องมือที่ทำจากวัสดุโลหะใช้ติดด้านนอกของผิวฟันด้านหน้าและมีการเปลี่ยนยางและปรับลวดทุก ๆ เดือนเพื่อเคลื่อนฟัน</p>

							<a class="btn btn-primary mt-5 mb-2" href="">Learn More <i class="fas fa-angle-right pl-1"></i></a>
						</div>
					</div>
				</div>
			</section>
			<section class="mt-5 mb-0 pb-0">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 class="mb-0 font-weight-semibold">โปรโมชั่น:</h2>
							<p class="lead mb-3">ราคาถูกๆ </p>
							<div class="divider divider-primary divider-small divider-small-center mb-3">
								<hr>
							</div>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-lg-8">
							<div class="carousel-areas mt-3 mb-0">
								<div class="owl-carousel owl-theme m-0" data-plugin-options="{'autoHeight': true, 'items': 1, 'margin': 10, 'nav': true, 'dots': false, 'stagePadding': 0}">
									<div>
										<a href="">
											<img alt="" class="img-fluid" src="https://www.cosdentbyslc.com/wp-content/uploads/2018/03/%E0%B8%88%E0%B8%B1%E0%B8%94%E0%B8%9F%E0%B8%B1%E0%B8%99-%E0%B8%A3%E0%B8%B2%E0%B8%84%E0%B8%B2%E0%B8%9E%E0%B8%B4%E0%B9%80%E0%B8%A8%E0%B8%A9-cosdent-by-slc.jpg">
										</a>
									</div>
									<div>
										<a href="">
											<img alt="" class="img-fluid" src="https://www.cosdentbyslc.com/wp-content/uploads/2018/03/veneer-protion-cosdent-by-slc-1.jpg">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			

			<div class="container">

				<div class="row mt-5 pt-3 counters counters-text-dark">
					<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
						<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
							<i class="fas fa-user"></i>
							<strong data-to="30000" data-append="+">0</strong>
							<label>Happy Clients</label>
							<p class="text-color-primary mb-3">They can’t be wrong</p>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
						<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
							<i class="fas fa-desktop"></i>
							<strong data-to="19">0</strong>
							<label>Premade HomePages</label>
							<p class="text-color-primary mb-3">Many more to come</p>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-4 mb-sm-0">
						<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
							<i class="fas fa-ticket-alt"></i>
							<strong data-to="2500" data-append="+">0</strong>
							<label>Answered Tickets</label>
							<p class="text-color-primary mb-3">Satisfaction guaranteed</p>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
							<i class="far fa-clock"></i>
							<strong data-to="3000" data-append="+">0</strong>
							<label>Development Hours</label>
							<p class="text-color-primary mb-3">Available to you for only $16</p>
						</div>
					</div>
				</div>

			</div>
		</div>

		<footer class="short" id="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<a href="" class="logo">
							<img alt="Porto Website Template" class="img-fluid" src="img/demos/digital-agency/logo-digital-agency.png">
						</a>
					</div>
					<div class="col-sm-3 col-lg-2 text-sm-right ml-sm-auto mb-0">
						<h5 class="mb-2">New York</h5>
						<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
					</div>
					<div class="col-sm-3 col-lg-2 text-sm-right">
						<h5 class="mb-2">Los Angeles</h5>
						<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<hr class="solid">
						<div class="row">
							<div class="col-lg-6">
								<p>© Copyright 2018. All Rights Reserved.</p>
							</div>
							<div class="col-lg-6 text-right">
								<ul class="social-icons float-right">
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
									<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>
								<span class="footer-email-custom float-right"><i class="far fa-envelope text-color-primary"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<!-- Vendor -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
	<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
	<script src="vendor/popper/umd/popper.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/common/common.min.js"></script>
	<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
	<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
	<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
	<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
	<script src="vendor/isotope/jquery.isotope.min.js"></script>
	<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/vide/vide.min.js"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="js/theme.js"></script>

	<!-- Current Page Vendor and Views -->
	<script src="js/views/view.contact.js"></script>

	<!-- Current Page Vendor and Views -->
	<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<!-- Theme Custom -->
	<script src="js/custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
	-->


</body>
</html>