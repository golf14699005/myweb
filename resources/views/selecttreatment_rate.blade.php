<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="css/demos/demo-digital-agency.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="css/skins/skin-digital-agency.css"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">

	<!-- Head Libs -->
	<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body>

	<div class="body border">
		<header id="header" class="header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '0'}">
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<a href="/home1">
										<img alt="Porto" width="131" height="40" src="img/demos/digital-agency/logo-digital-agency.png">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-stripe">
									<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li>
														<a class="nav-link " href="/home1">
															Home
														</a>
													</li>
													<li>
														<a class="nav-link" href="/adddaytable_user">
															ตารางนัด
														</a>
													</li>
													
													<li>
														<a class="nav-link active" href="/selecttreatment_rate">
															อัตราการรักษา
														</a>
													</li>
													<li>
														<a class="nav-link" href="/selectuser">
															แก้ไขข้อบุคคน
														</a>
													</li>
													<li>
														<a class="nav-link" href="/adduser">
															เพิ่มข้อบุคคน
														</a>
													</li>
													<li>
														<a class="nav-link" href="/login">
															ออกจากระบบ
														</a>
													</li>
											</ul>
										</nav>
									</div>
									
									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div role="main" class="main mt-lg-5  p-5 pt-5 pb-5  ">
			<div class="container-fluid  p-5 pt-5 pb-5  ">
				<div class="row p-1">
					
					<div class="container   ">
						<div class="form-row ">
							<h2 class="display-4 mt-2 mb-3 col-lg-4 "> อัตราการรักษา</h2>
							<a href="/addtreatment_rate" class="offset-6   pt-4" >
								<button type="" class="btn btn-primary "> เพิ่มข้อมูล </button>
							</a>
						</div>
						
						<div class="row">
							<div class="form-row w-100">
								<div class="table-responsive">
									<table class="table table-hover">
										<thead>
											<th>#</th>
											<th>รายการ</th>
											<th>price</th>
											<th></th>
											<th></th>
										</thead>	
										<tbody>
											<?php
											if(count($users) > 0){
											$i = 1 ;
											foreach ($users as $user) {
											echo "<tr>";
												echo "<td>".$i."</td>";
												echo "<td>".$user->name."</td>";
												echo "<td>".$user->price."</td>";
												
												echo "<td><a href='delete_data_treatment_rate/".$user->TID."' title=''>Delete</a></td>";
												echo "<td><a href='edit_data_treatment_rate/".$user->TID."' title=''>Edit</a></td>";

											echo "</tr>";
											$i++;
										}
									}
									else
									{
										echo "<tr><td align='center' colspan='4'>Not Found Data </td></tr>";
									}

									?>				
								</tbody>

							</table>					
						</div>				
					</div>			
				</div>
			</div>

		</div>
	</div>






	<footer class="short" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<a href="demo-digital-agency.html" class="logo">
						<img alt="Porto Website Template" class="img-fluid" src="img/demos/digital-agency/logo-digital-agency.png">
					</a>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right ml-sm-auto mb-0">
					<h5 class="mb-2">New York</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right">
					<h5 class="mb-2">Los Angeles</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<hr class="solid">
					<div class="row">
						<div class="col-lg-6">
							<p>© Copyright 2018. All Rights Reserved.</p>
						</div>
						<div class="col-lg-6 text-right">
							<ul class="social-icons float-right">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
							<span class="footer-email-custom float-right"><i class="far fa-envelope text-color-primary"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>


	<!-- Vendor -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
	<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
	<script src="vendor/popper/umd/popper.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/common/common.min.js"></script>
	<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
	<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
	<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
	<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
	<script src="vendor/isotope/jquery.isotope.min.js"></script>
	<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/vide/vide.min.js"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="js/theme.js"></script>

	<!-- Current Page Vendor and Views -->
	<script src="js/views/view.contact.js"></script>

	<!-- Current Page Vendor and Views -->
	<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<!-- Theme Custom -->
	<script src="js/custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="js/theme.init.js"></script>


</body>
</html>