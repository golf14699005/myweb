<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	
	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="css/demos/demo-digital-agency.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="css/skins/skin-digital-agency.css"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">

	<!-- Head Libs -->
	<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body>

	
	<div class="body border">
		<header id="header" class="header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '0'}">
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<a href="/home1">
										<img alt="Porto" width="131" height="40" src="img/demos/digital-agency/logo-digital-agency.png">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-stripe">
									<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li>
														<a class="nav-link " href="/home1">
															Home
														</a>
													</li>
													<li>
														<a class="nav-link" href="/adddaytable_user">
															ตารางนัด
														</a>
													</li>
													
													<li>
														<a class="nav-link" href="/selecttreatment_rate">
															อัตราการรักษา
														</a>
													</li>
													<li>
														<a class="nav-link" href="/selectuser">
															แก้ไขข้อบุคคน
														</a>
													</li>
													<li>
														<a class="nav-link active" href="/adduser">
															เพิ่มข้อบุคคน
														</a>
													</li>
													<li>
														<a class="nav-link" href="/login">
															ออกจากระบบ
														</a>
													</li>
											</ul>
										</nav>
									</div>

									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
	</div>
	<div role="main" class="main mt-lg-5  p-5">


		<div class="container-fluid ">

			<div class="row p-1">
				<div class=" col-lg-2 border">
					<ul class="nav nav-pills flex-column">
						<li class="nav-item">
							<span class=""> DASHBOARDS</span>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="#">พนักงาน</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " href="addDoctor">หมอ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " href="addCustomer">ลูกค้า</a>
						</li>

					</ul>
				</div>
				<div class="col-lg-4  p-4 ">
					<form action="/insert_data" method="POST">
						{{ csrf_field() }}
						<div class="form-group">
							<label >Username</label>
							<input type="text" class="form-control" id="username"  placeholder="Enter Username" name="username">	
						</div>
						<div class="form-group">
							<label >Password</label>
							<input type="password" class="form-control" id="password"  placeholder="Enter Password" name="password">		
						</div>
						<div class="form-group">
							<label >First Name</label>
							<input type="text" class="form-control" id="fname"  placeholder="Enter First Name" name="fname">				
						</div>
						<div class="form-group">
							<label >Last Name</label>
							<input type="text" class="form-control" id="lname"  placeholder="Enter Last Name" name="lname">				
						</div>
						<div class="form-group">
							<label >Address</label>
							<input type="text" class="form-control" id="add"  placeholder="Enter Address" name="add">				
						</div>
						<div class="form-group">
							<label >Birthday</label>
							<input type="date" class="form-control" id="bday"  placeholder="Enter Birthday" name="bday">				
						</div>
						<div class="form-group">
							<label >Telephone</label>
							<input type="text" class="form-control" id="phone"  placeholder="Enter Telephone" name="phone">				
						</div>
						
						<div class="form-group">
							<label >ใส่รูป</label>
							<input type="file" class="form-control-file" id="photo" name="photo">
						</div>
						
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>			

				</div>

			</div>
		</div>

	</div>

	<footer class="short" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<a href="demo-digital-agency.html" class="logo">
						<img alt="Porto Website Template" class="img-fluid" src="img/demos/digital-agency/logo-digital-agency.png">
					</a>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right ml-sm-auto mb-0">
					<h5 class="mb-2">New York</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right">
					<h5 class="mb-2">Los Angeles</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<hr class="solid">
					<div class="row">
						<div class="col-lg-6">
							<p>© Copyright 2018. All Rights Reserved.</p>
						</div>
						<div class="col-lg-6 text-right">
							<ul class="social-icons float-right">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
							<span class="footer-email-custom float-right"><i class="far fa-envelope text-color-primary"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>


	<!-- Vendor -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
	<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
	<script src="vendor/popper/umd/popper.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/common/common.min.js"></script>
	<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
	<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
	<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
	<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
	<script src="vendor/isotope/jquery.isotope.min.js"></script>
	<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="vendor/vide/vide.min.js"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="js/theme.js"></script>

	<!-- Current Page Vendor and Views -->
	<script src="js/views/view.contact.js"></script>

	<!-- Current Page Vendor and Views -->
	<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<!-- Theme Custom -->
	<script src="js/custom.js"></script>

	<!-- Theme Initialization Files -->
	<script src="js/theme.init.js"></script>


</body>
</html>