<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Demo Digital Agency | Porto - Responsive HTML5 Template 6.2.0</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="vendor/animate/animate.min.css">
	<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/theme.css">
	<link rel="stylesheet" href="css/theme-elements.css">
	<link rel="stylesheet" href="css/theme-blog.css">
	<link rel="stylesheet" href="css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="css/demos/demo-digital-agency.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="css/skins/skin-digital-agency.css"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="css/custom.css">

	<!-- Head Libs -->
	<script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body>

	<div class="body">
		<header id="header" class="header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '0'}">
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<a href="/home1">
										<img alt="Porto" width="131" height="40" src="img/demos/digital-agency/logo-digital-agency.png">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-stripe">
									<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li>
													<a class="nav-link " href="/home1">
														Home
													</a>
												</li>
												<li>
													<a class="nav-link active" href="/adddaytable_user">
														ตารางนัด
													</a>
												</li>

												<li>
													<a class="nav-link" href="/selecttreatment_rate">
														อัตราการรักษา
													</a>
												</li>
												<li>
													<a class="nav-link" href="/selectuser">
														แก้ไขข้อบุคคน
													</a>
												</li>
												<li>
													<a class="nav-link" href="/adduser">
														เพิ่มข้อบุคคน
													</a>
												</li>
												<li>
													<a class="nav-link" href="/login">
														ออกจากระบบ
													</a>
												</li>
											</ul>
										</nav>
									</div>
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div role="main" class="main">
			<div class="body">

				<div role="main" class="main  p-5 pt-5 pb-5 ">
					<div class="container  p-5 pt-5 pb-5    ">
						<div class="row p-1 mt-4">

							<div class="container border  col-8">
								<div class="form-row   ">
									<h2 class="display-4 mt-2 mb-3 ">ตารางเวลาของคุณ</h2>
								</div>

								<div class="row">
									<div class="form-row w-100">
										<div class="table-responsive">
											<table class="table table-hover" >
												<thead >
													<th >id</th>
													<th>วัน</th>
													<th>เวลา</th>

													<th>ลบ</th>
												</thead>	
												<tbody >
													<?php
													if(count($users) > 0){
														$i = 1 ;
														foreach ($users as $user) {
															echo "<tr>";
															echo "<td >".$user->id_doc."</td>";
															echo "<td>".$user->day."</td>";
															echo "<td>".$user->time."</td>";

															echo "<td><a href='/deletedaytable_user/".$user->id."' title=''>Delete</a></td>";

															echo "</tr>";
															$i++;
														}
													}
													else
													{
														echo "<tr><td align='center' colspan='4'>ไม่มีข้อมูล</td></tr>";
													}

													?>				
												</tbody>

											</table>					
										</div>				
									</div>			
								</div>
							</div>
							<div class=" col-4 border" align="center">
								<form action="/insertday_user" method="POST" accept-charset="utf-8">
									{{ csrf_field() }}
									<input type="hidden" name="id" value="<?=$ans[0]->DID?>">
									<div class="form-row ">
										<div class="col-6 mt-1" align="center">
											<label>เลือกวัน</label>
										</div>
										<div class="col-6 mt-1" align="center">
											<label>เลือกวัน</label>
										</div>
									</div>
									<div class="form-row  ">

										<div class="btn-group  p-5 col-6 ">
											<select name="day"class="btn btn-warning dropdown-toggle" >


												<option  class="dropdown-item" value="วันอาทิตย์">วันอาทิตย์</option >
												<option  class="dropdown-item" value="วันจันทร์">วันจันทร์</option >
												<option  class="dropdown-item" value="วันอังคาร">วันอังคาร</option >
												<option  class="dropdown-item" value="วันพุธ">วันพุธ</option >
												<option  class="dropdown-item" value="วันพฤหัสบดี">วันพฤหัสบดี</option >
												<option  class="dropdown-item" value="วันศุกร์">วันศุกร์</option >
												<option  class="dropdown-item" value="วันเสาร์">วันเสาร์</option >

											</select>
										</div>

										<div class="btn-group  p-5 col-6 ">
											<select name="time" class="btn btn-info dropdown-toggle">


												<option  class="dropdown-item" value="เช้า">เช้า</option >
												<option  class="dropdown-item" value="บ่าย">บ่าย</option >
												<option  class="dropdown-item" value="ทั้งวัน">ทั้งวัน</option >

											</select>
										</div>
									</div>

									<select name="cid"class="btn btn-warning dropdown-toggle" >
										<?php
										if(count($ans) > 0){

											foreach ($ans as $user) {
												echo "<option  class='dropdown-item' value='".$user->DID."''>id : ".$user->DID." ".$user->Dfirst_name."</option >";
											}
										}
										?>

									</select><br>
									<br>
									<br>
									<button type="submit"  class="btn btn-outline-success">Submit</button>
								</form>

							</div>

						</div>
					</div>
				</div>

				<footer class="short" id="footer">
					<div class="container">
						<div class="row">
							<div class="col-sm-2">
								<a href="demo-digital-agency.html" class="logo">
									<img alt="Porto Website Template" class="img-fluid" src="img/demos/digital-agency/logo-digital-agency.png">
								</a>
							</div>
							<div class="col-sm-3 col-lg-2 text-sm-right ml-sm-auto mb-0">
								<h5 class="mb-2">New York</h5>
								<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
							</div>
							<div class="col-sm-3 col-lg-2 text-sm-right">
								<h5 class="mb-2">Los Angeles</h5>
								<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<hr class="solid">
								<div class="row">
									<div class="col-lg-6">
										<p>© Copyright 2018. All Rights Reserved.</p>
									</div>
									<div class="col-lg-6 text-right">
										<ul class="social-icons float-right">
											<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
											<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
											<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
										<span class="footer-email-custom float-right"><i class="far fa-envelope text-color-primary"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>

			<!-- Vendor -->
			<script src="vendor/jquery/jquery.min.js"></script>
			<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
			<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
			<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
			<script src="vendor/popper/umd/popper.min.js"></script>
			<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
			<script src="vendor/common/common.min.js"></script>
			<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
			<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
			<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
			<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
			<script src="vendor/isotope/jquery.isotope.min.js"></script>
			<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
			<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
			<script src="vendor/vide/vide.min.js"></script>

			<!-- Theme Base, Components and Settings -->
			<script src="js/theme.js"></script>

			<!-- Current Page Vendor and Views -->
			<script src="js/views/view.contact.js"></script>

			<!-- Current Page Vendor and Views -->
			<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
			<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

			<!-- Theme Custom -->
			<script src="js/custom.js"></script>

			<!-- Theme Initialization Files -->
			<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
	-->


</body>
</html>