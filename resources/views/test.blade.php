 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title></title>
 	<link rel="stylesheet" href="">
 	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
 	<script src="{{ asset('js/jquery.js')}}" type="text/javascript" ></script>
 	<style>
 	tr:hover {background-color: #FFFFE0}
 	td:hover{background-color:#CD5C5C;cursor: pointer}
 	.selected{background-color: #00FF00;color: #00FF00 ;font-weight: bold}	
 	.hcolor{background-color: #AFEEEE}
 	.Dcolor{background-color: #E0FFFF}
 </style>
</head>
<body>
	<div class="container mb-5">
		<h1 class="display-4 mt-4 mb-5"> ลงเวลา</h1>
		<div class="row">
			<div class="form-row w-100">
				<div class="table-responsive">
					<table class="table" id="table" border="1">
						
						<thead > 

							<th class="hcolor">วัน/เวลา</th>
							<th class="hcolor">8.00</th>
							<th class="hcolor">9.00</th>
							<th class="hcolor">10.00</th>
							<th class="hcolor">11.00</th>
							<th class="hcolor">12.00</th>
							<th class="hcolor">13.00</th>
							<th class="hcolor">14.00</th>
							<th class="hcolor">15.00</th>
							<th class="hcolor">16.00</th>
							<th class="hcolor">17.00</th>
						</thead>
						
						<tbody class="Dcolor">
							<tr>
								<th class="hcolor">Sunday</th>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>พัก</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th class="hcolor">Monday</th>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>พัก</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th class="hcolor">Tuesday</th>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>พัก</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th class="hcolor">Wednesday</th>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>พัก</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th class="hcolor">Thursday</th>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>พัก</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th class="hcolor">Friday</th>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>พัก</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th class="hcolor">Saturday</th>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td>พัก</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>			

						</tbody>
					</table>					
				</div>				
			</div>			
		</div>
	</div>


	
	<script>
		var data = [[1,2],[1,3],[1,4]];
		var table = document.getElementById("table"),rIndex,cIndex;
		var n = 0;
		

		for(var i = 0; i < table.rows.length; i++)
		{
                // row cells

                for(var j = 0; j < table.rows[i].cells.length; j++)
                {
                	if(i < 1 || j < 1)
                	{

                	}

                	else if(j != 5  )
                	{
                		table.rows[i].cells[j].onclick = function()
                		{
                			this.classList.toggle("selected");
                			rIndex = this.parentElement.rowIndex;
                			cIndex = this.cellIndex+1;
                			//document.getElementsByClassName("selected").href = "/home";
                			//a[n][rIndex][cIndex];

                			console.log("Row : "+rIndex+" , Cell : "+cIndex +" n = " + n);
                			n++;
                		};

                	}

                }
            }

        </script> 

        <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript" ></script>

        <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript" ></script>
    </body>
    </html>