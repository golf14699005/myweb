<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="{{ asset('img/apple-touch-icon.png')}}">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/fontawesome-all.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/animate/animate.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.min.css')}}">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{ asset('css/theme.css')}}">
	<link rel="stylesheet" href="{{ asset('css/theme-elements.css')}}">
	<link rel="stylesheet" href="{{ asset('css/theme-blog.css')}}">
	<link rel="stylesheet" href="{{ asset('css/theme-shop.css')}}">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/settings.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/layers.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/navigation.css')}}">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="{{ asset('css/demos/demo-digital-agency.css')}}">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="{{ asset('css/skins/skin-digital-agency.css')}}"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="{{ asset('css/custom.css')}}">

	<!-- Head Libs -->
	<script src="{{ asset('vendor/modernizr/modernizr.min.js')}}"></script>
	

</head>
<body>

	<div class="body">
		<header id="header" class="header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '0'}">
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<?php  
									echo "<a  href='/customerhome/".$ans[0]->Cus_ID."'>";?>
									<img alt="Porto" width="131" height="40" src="{{ asset('img/demos/digital-agency/logo-digital-agency.png')}}">
									<?php "</a>";?>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-stripe">
									<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li>
													<?php  
													echo "<a class='nav-link '   href='/customerhome/".$ans[0]->Cus_ID."'>							
													Home
													</a>";
													?>
													
												</li>

												<li>
													<?php  
													echo "<a  class='nav-link active' href='/Schedule/".$ans[0]->Cus_ID."'>							
													นัดหมอ
													</a>";
														//echo "<a href='delete_data_customer/".$ans."' title=''>Delete</a>";
													?>
													
												</li>
												<li>
													<?php  
													echo "<a  class='nav-link' href='/deedate/".$ans[0]->Cus_ID."'>							
													ดูตารางนัด
													</a>";
														//echo "<a href='delete_data_customer/".$ans."' title=''>Delete</a>";
													?>
												</li>
												
												<li>
													<?php  
													echo "<a  class='nav-link' href='/checktabledoc/".$ans[0]->Cus_ID."'>							
													ตารางเข้างานหมอ
													</a>";
														//echo "<a href='delete_data_customer/".$ans."' title=''>Delete</a>";
													?>
												</li>
												<li>
													<?php  
													echo "<a  class='nav-link' href='/selecttreatment_rate_cus/".$ans[0]->Cus_ID."'>							
													เช็คอคาราการรักษา
													</a>";
														//echo "<a href='delete_data_customer/".$ans."' title=''>Delete</a>";
													?>
													
												</li>
												<li class="dropdown">
													<?php
													if(count($ans) > 0){
														
														foreach ($ans as $user) {
															echo "<a class='dropdown-item dropdown-toggle'>".$user->Cuser." </a>";	
															echo "<ul class='dropdown-menu'>
															<li><a class='dropdown-item' href='#'>".$user->Cfirst_name."</a></li>
															<li><a class='dropdown-item' href='#'>".$user->Clast_name."</a></li>
															<li><a class='dropdown-item' href='/edit_customer/".$user->Cus_ID."'>แก้ไขข้อมูลส่วนตัว</a></li>
															<li><a class='dropdown-item' href='/login'>ออกจากระบบ</a></li>
															</ul>";
														}
													}
													else
													{
														echo "<tr><td align='center' colspan='4'>Not Found Data </td></tr>";
													}

													?>	
												</li>
											</ul>
										</nav>
									</div>

									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div role="main" class="main  p-5 pt-5 pb-5 ">
		<div class="container  p-5 pt-5 pb-5    ">
			<div class="row p-1 mt-4">

				<div class="container border col-lg-6 ">
					<div class="form-row   ">
						<h2 class="display-4 mt-2 mb-3 ">ตารางเวลาหมอ</h2>
					</div>

					<div class="row">
						<div class="form-row w-100">
							<div class="table-responsive">
								<table class="table table-hover" >
									<thead >
										
										<th> id หมอ</th>
										<th>วัน</th>
										<th>เวลา</th>
									</thead>	
									<tbody >
										<?php
										
										if(count($users) > 0){
											
											foreach ($users as $user) {
												echo "<tr>";

												echo "<td >".$user->id_doc."</td>";
												echo "<td>".$user->day."</td>";
												echo "<td>".$user->time."</td>";
												echo "</tr>";
												
											}
										}
										else
										{
											echo "<tr><td align='center' colspan='4'>ไม่มีข้อมูล</td></tr>";
										}

										?>				
									</tbody>

								</table>					
							</div>				
						</div>			
					</div>
				</div>
				<div class=" col-6 border" align="center">
					<form action="/inserdate_table" method="post" accept-charset="utf-8">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="<?=$ans[0]->Cus_ID?>">
						<div class="form-row mt-5">
							<div class="col-6 mt-1" align="center">
								<label>เลือกวัน</label>
								<input type="date" name=TDate">
							</div>
							<div class="col-6 mt-1" align="center">
								
								<label for="appt-time">เลือกเวลา 8:00 to 17:00 </label>
								<input id="appt-time" type="time" name="TTime"
								min="08:00" max="17:00">
								<span class="validity"></span>
							</div>
						</div>
						<label>เลือก ID หมอ</label>
						<div class="btn-group mt-3 p-5    " >

							<select name="did"class="btn btn-warning dropdown-toggle" >
								<?php
								if(count($doc) > 0){

									foreach ($doc as $user) {
										echo "<option  class='dropdown-item' value='".$user->DID."''>".$user->DID."</option >";
									}
								}
								?>

							</select>
						</div>
						
						<div class="mt-3 p-5 ">
							<button type="submit"  class="btn btn-outline-success">Submit</button>
						</div>

						


						



					</form>

				</div>
				

			</div>
		</div>
	</div>

	<footer class="short" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<a href="demo-digital-agency.html" class="logo">
						<img alt="Porto Website Template" class="img-fluid" src="img/demos/digital-agency/logo-digital-agency.png">
					</a>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right ml-sm-auto mb-0">
					<h5 class="mb-2">New York</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right">
					<h5 class="mb-2">Los Angeles</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<hr class="solid">
					<div class="row">
						<div class="col-lg-6">
							<p>© Copyright 2018. All Rights Reserved.</p>
						</div>
						<div class="col-lg-6 text-right">
							<ul class="social-icons float-right">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
							<span class="footer-email-custom float-right"><i class="far fa-envelope text-color-primary"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>


<!-- Vendor -->
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.appear/jquery.appear.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{ asset('vendor/jquery-cookie/jquery-cookie.min.js')}}"></script>
<script src="{{ asset('vendor/popper/umd/popper.min.js')}}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/common/common.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.validation/jquery.validation.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
<script src="{{ asset('vendor/isotope/jquery.isotope.min.js')}}"></script>
<script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ asset('vendor/vide/vide.min.js')}}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('js/theme.js')}}"></script>

<!-- Current Page Vendor and Views -->
<script src="{{ asset('js/views/view.contact.js')}}"></script>

<!-- Current Page Vendor and Views -->
<script src="{{ asset('vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- Theme Custom -->
<script src="{{ asset('js/custom.js')}}"></script>

<!-- Theme In{{ asset('itialization Files -->
<script src="js/theme.init.js"></script>

</body>
</html>