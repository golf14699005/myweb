<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="{{ asset('img/apple-touch-icon.png')}}">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/fontawesome-all.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/animate/animate.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.min.css')}}">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{ asset('css/theme.css')}}">
	<link rel="stylesheet" href="{{ asset('css/theme-elements.css')}}">
	<link rel="stylesheet" href="{{ asset('css/theme-blog.css')}}">
	<link rel="stylesheet" href="{{ asset('css/theme-shop.css')}}">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/settings.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/layers.css')}}">
	<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/navigation.css')}}">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="{{ asset('css/demos/demo-digital-agency.css')}}">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="{{ asset('css/skins/skin-digital-agency.css')}}"> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="{{ asset('css/custom.css')}}">

	<!-- Head Libs -->
	<script src="{{ asset('vendor/modernizr/modernizr.min.js')}}"></script>
	

</head>
<body>

	<div class="body">
		<header id="header" class="header-semi-transparent" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '0'}">
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<?php  
									echo "<a  href='/doctrohome/".$ans[0]->DID."'>";?>
									<img alt='Porto' width='131' height='40' src='{{ asset('img/demos/digital-agency/logo-digital-agency.png')}}''>
									<?php "</a>";?>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div class="header-nav header-nav-stripe">
									<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li>
													<?php  
													echo "<a  class='nav-link' href='/doctrohome/".$ans[0]->DID."'>							
													Home</a>";
													?>
												</li>
												<li>
													<a class="nav-link active" href="#">
														บันทึกผลการรักษา
													</a>
												</li>
												<li>
													<?php  
													echo "<a  class='nav-link' href='/selectcustomer_doc/".$ans[0]->DID."'>							
													รายชื่อลูกค้า
													</a>";
														//echo "<a href='delete_data_customer/".$ans."' title=''>Delete</a>";
													?>
													
												</li>
												<li>
													<?php  
													echo "<a  class='nav-link' href='/selecttreatment_rate_doc/".$ans[0]->DID."'>							
													เช็คราคาการรักษา
													</a>";
														//echo "<a href='delete_data_customer/".$ans."' title=''>Delete</a>";
													?>
													
												</li>
												<li>
													<?php  
													echo "<a  class='nav-link' href='/adddaytable/".$ans[0]->DID."'>							
													ลงเวลานัด
													</a>";
														//echo "<a href='delete_data_customer/".$ans."' title=''>Delete</a>";
													?>
													
												</li>
												
												<li class="dropdown">
													<?php
													if(count($ans) > 0){
														
														foreach ($ans as $user) {
															echo "<a class='dropdown-item dropdown-toggle'>".$user->Duser." </a>";	
															echo "<ul class='dropdown-menu'>
															<li><a class='dropdown-item' href='#'>".$user->Dfirst_name."</a></li>
															<li><a class='dropdown-item' href='#'>".$user->Dlast_name."</a></li>
															<li><a class='dropdown-item' href='/edit_doctor/".$user->DID."'>แก้ไขข้อมูลส่วนตัว</a></li>
															<li><a class='dropdown-item' href='/login'>ออกจากระบบ</a></li>
															</ul>";
														}
													}
													else
													{
														echo "<tr><td align='center' colspan='4'>Not Found Data </td></tr>";
													}

													?>	
												</li>
											</ul>
										</nav>
									</div>

									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
	</div>
	<div role="main" class="main   p-5 pt-5 pb-5  ">
		<div class="container  p-5 pt-5 pb-5    ">
			<div class="row p-1 mt-4">

				<div class="container border  col-6">
					<div class="form-row   ">
						<label class="display-4 mt-2 mb-3 ">รายชื่อลูกค้า</label>
					</div>

					<div class="row">
						<div class="form-row w-100">
							<div class="table-responsive">
								<table class="table table-hover" >
									<thead >
										<th >id</th>
										<th>ชื่อ</th>
										<th>นามสกุล</th>
										<th>ยอดคงเหลือ</th>
									</thead>	
									<tbody >
										<?php
										if(count($cus) > 0){
											$i = 1 ;
											foreach ($cus as $user) {
												echo "<tr>";
												echo "<td >".$user->Cus_ID."</td>";
												echo "<td>".$user->Cfirst_name."</td>";
												echo "<td>".$user->Clast_name."</td>";
												echo "<td>".$user->money."</td>";
											//	echo "<td><a href='/deletedaytable/".$user->id."/".$ans[0]->DID."' title=''>Delete</a></td>";

												echo "</tr>";
												$i++;
											}
										}
										else
										{
											echo "<tr><td align='center' colspan='4'>ไม่มีข้อมูล</td></tr>";
										}

										?>				
									</tbody>

								</table>					
							</div>				
						</div>			
					</div>
				</div>
				<div class=" col-6 border" align="center">
					<form action="/insertrecord" method="POST" accept-charset="utf-8">
						{{ csrf_field() }}
						<input type="hidden" name="did" value="<?=$ans[0]->DID?>">
						<div class="form-row ">
							<div class="col-6 mt-1" align="center">
								<label>เลือกรายชื่อ</label>
							</div>
							<div class="col-6 mt-1" align="center">
								<label>เลือกรายการ</label>
							</div>

						</div>
						<div class="form-row">

							<div class="btn-group  p-5 col-6  " >

								<select name="cid"class="btn btn-warning dropdown-toggle" >
									<?php
									if(count($cus) > 0){

										foreach ($cus as $user) {
											echo "<option  class='dropdown-item' value='".$user->Cus_ID."''>".$user->Cfirst_name."</option >";
										}
									}
									?>
									
								</select>
							</div>

							<div class="btn-group  p-5 col-6 ">
								<select name="tid" class="btn btn-info dropdown-toggle">
									<?php
									if(count($tre) > 0){

										foreach ($tre as $user) {
											echo "<option  class='dropdown-item' value='".$user->TID."''>".$user->name." ราคา ".$user->price."</option >";
										}
									}
									?>
								</select>
							</div>
						</div>
						<label>จำนวนเงิน</label>
						<input type="text" name="money">

						<button type="submit"  class="btn btn-outline-success">Submit</button>
					</form>

				</div>

			</div>
		</div>
	</div>

	<footer class="short" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<a href="demo-digital-agency.html" class="logo">
						<img alt="Porto Website Template" class="img-fluid" src="img/demos/digital-agency/logo-digital-agency.png">
					</a>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right ml-sm-auto mb-0">
					<h5 class="mb-2">New York</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
				<div class="col-sm-3 col-lg-2 text-sm-right">
					<h5 class="mb-2">Los Angeles</h5>
					<span class="phone text-2"><i class="fas fa-phone text-color-primary"></i> (800) 123-4567</span>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<hr class="solid">
					<div class="row">
						<div class="col-lg-6">
							<p>© Copyright 2018. All Rights Reserved.</p>
						</div>
						<div class="col-lg-6 text-right">
							<ul class="social-icons float-right">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
							<span class="footer-email-custom float-right"><i class="far fa-envelope text-color-primary"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>


<!-- Vendor -->
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.appear/jquery.appear.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{ asset('vendor/jquery-cookie/jquery-cookie.min.js')}}"></script>
<script src="{{ asset('vendor/popper/umd/popper.min.js')}}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/common/common.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.validation/jquery.validation.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
<script src="{{ asset('vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
<script src="{{ asset('vendor/isotope/jquery.isotope.min.js')}}"></script>
<script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ asset('vendor/vide/vide.min.js')}}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('js/theme.js')}}"></script>

<!-- Current Page Vendor and Views -->
<script src="{{ asset('js/views/view.contact.js')}}"></script>

<!-- Current Page Vendor and Views -->
<script src="{{ asset('vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- Theme Custom -->
<script src="{{ asset('js/custom.js')}}"></script>

<!-- Theme In{{ asset('itialization Files -->
<script src="js/theme.init.js"></script>

</body>
</html>