<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('check-connect',function(){
	if(DB::connection()->getDatabaseName())
	{
		return "Yes! successfully connected to the DB: " . DB:: connection()->getDatabaseName();

	}else
	{
		return 'Connection False !! ';
	}
});

Route::post('/insertday_user','controllerdbuser@insertday_user');
Route::get('/deletedaytable_user/{id}','controllerdbuser@deletedaytable_user');
Route::get('/adddaytable_user','controllerdbuser@adddaytable_user');
Route::get('/home', 'Usercontroller@home');
Route::get('/home1', 'Usercontroller@home1');
Route::get('/about', 'Usercontroller@about');
Route::get('/services', 'Usercontroller@services');
Route::get('/selecttreatment_rate', 'controllerdbuser@selecttreatment_rate');

Route::get('/adduser', 'Usercontroller@adduser');
Route::get('/addDoctor', 'Usercontroller@addDoctor');
Route::get('/addCustomer', 'Usercontroller@addCustomer');

Route::get('/addtreatment_rate', 'controllerdbuser@addtreatment_rate');

Route::post('/insert_data1', 'Usercontroller@insert_data');
//Route::post('/addSchedule', 'Usercontroller@addSchedule');
Route::post('/test', 'controllerdbuser@test');

Route::post('/insert_data', 'controllerdbuser@insert_data');
Route::post('/insert_data_doctor', 'controllerdbuser@insert_data_doctor');
Route::post('/insert_data_customer', 'controllerdbuser@insert_data_customer');

Route::post('/insert_data_addtreatment_rate', 'controllerdbuser@insert_data_addtreatment_rate');

Route::get('/selectuser', 'controllerdbuser@selectuser');
Route::get('/selectdoctor', 'controllerdbuser@selectdoctor');
Route::get('/selectcustomer', 'controllerdbuser@selectcustomer');


Route::get('/delete_data_user/{id}', 'Usercontroller@delete_data_user');
Route::get('/delete_data_doctor/{id}', 'Usercontroller@delete_data_doctor');
Route::get('/delete_data_customer/{id}', 'Usercontroller@delete_data_customer');
Route::get('/delete_data_treatment_rate{id}','controllerdbuser@delete_data_treatment_rate');

Route::get('/edit_data_user/{id}', 'controllerdbuser@edit_data_user');
Route::get('/edit_data_doctor/{id}', 'controllerdbuser@edit_data_doctor');
Route::get('/edit_data_customer/{id}', 'controllerdbuser@edit_data_customer');
Route::get('/delete_data_treatment_rate/{id}', 'controllerdbuser@delete_data_treatment_rate');
Route::get('/edit_data_treatment_rate/{id}', 'controllerdbuser@edit_data_treatment_rate');

Route::post('/update_data_user', 'controllerdbuser@update_data_user');
Route::post('/update_data_doctor', 'controllerdbuser@update_data_doctor');
Route::post('/update_data_customer', 'controllerdbuser@update_data_customer');
Route::post('/update_data_treatment_rate' ,'controllerdbuser@update_data_treatment_rate');

Route::post('/test3', 'controllerdbuser@test3');
Route::get('/login', 'Usercontroller@login');
Route::post('/checkLogin', 'controllerdbuser@checkLogin');

Route::post('customerview', 'customerController@customerview');
Route::get('/edit_customer/{id}', 'customerController@edit_customer');
Route::post('/update_data_customer_cus','customerController@update_data_customer');
Route::get('/edit_doctor/{id}','doctorController@edit_doctor');
Route::post('/update_doctor','doctorController@update_doctor');
Route::get('/selectcustomer_doc/{id}' ,'doctorController@selectcustomer_doc');
Route::get('/doctrohome/{id}','doctorController@doctrohome');
Route::get('/selecttreatment_rate_doc/{id}','doctorController@selecttreatment_rate_doc');
Route::get('/adddaytable/{id}','doctorController@adddaytable');
Route::post('/insertday','doctorController@insertday');
Route::get('/deletedaytable/{id}/{idd}','doctorController@deletedaytable');
Route::get('/addredord/{id}','doctorController@addredord');
Route::post('/insertrecord','doctorController@insertrecord');
Route::get('/checktabledoc/{id}','customerController@checktabledoc');
Route::get('/customerhome/{is}', 'customerController@customerhome');
Route::get('/selecttreatment_rate_cus/{id}', 'customerController@selecttreatment_rate_cus');
Route::get('/Schedule/{id}','customerController@Schedule');
Route::post('/inserdate_table','customerController@inserdate_table');
Route::get('/deedate/{id}','customerController@deedate');
Route::get('/delete_data_schedule/{id}/{idd}','customerController@delete_data_schedule');
Route::get('/edit_data_schedule/{id}/{idd}','customerController@edit_data_schedule');